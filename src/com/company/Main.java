package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader br = new BufferedReader(new FileReader("ant.in"));
        String[] s = br.readLine().split(" ");
        int dimensionsNumber = Integer.parseInt(s[0]);
        int automatonStates = Integer.parseInt(s[1]);
        int maxStepsNumber = Integer.parseInt(s[2]);
        Automaton automaton = new Automaton(automatonStates);
        int maxStatesNumber = automaton.validCharacters.size() * automaton.validCharacters.size() * automatonStates * automatonStates;
        int[] statesNumbers = new int[automatonStates];
        int maxApplesNumber=0;
        char[][] input = new char[dimensionsNumber][dimensionsNumber];
        for (int i=0; i<dimensionsNumber;i++){
            input[i] = br.readLine().toCharArray();
        }
        for (int i=0; i<dimensionsNumber; i++){
            for (int j=0; j<dimensionsNumber; j++){
                if (input[i][j]=='*'){
                    maxApplesNumber++;
                }
            }
        }
        long tactsNumber = 0;
        long maxTactsNumber = (long) Math.pow(9*automatonStates*automatonStates,automatonStates);
        while (tactsNumber != maxTactsNumber) {
            for (int i = 0; i < automatonStates; i++) {
                automaton.states[i] = automaton.genState((tactsNumber / ((int) Math.pow(9*automatonStates * automatonStates, i)))%
                        (9*automatonStates*automatonStates), i);
            }
            int currentState = 0;
            int currentRow = 0;
            int currentColumn = 0;
            int m = 0;
            int n = 1;
            int applesNumber =0;
            char[][] currentInput = new char[dimensionsNumber][dimensionsNumber];
            for (int i=0; i<dimensionsNumber; i++){
                for (int j=0; j<dimensionsNumber;j++){
                     currentInput [i][j] = input[i][j];
                }
            }
            for (int k = 0; k < maxStepsNumber; k++) {
                Automaton.State.Transition transition;
                if (currentInput[(currentRow + m)>-1?(currentRow+m)%dimensionsNumber : dimensionsNumber-1 ]
                        [(currentColumn + n)>-1?(currentColumn+n)%dimensionsNumber : dimensionsNumber-1] == '*') {
                    transition = automaton.states[currentState].ifApple;
                } else {
                    transition = automaton.states[currentState].noApple;
                }
                System.out.println((currentRow + m)>-1?(currentRow+m)%dimensionsNumber : dimensionsNumber-1);
                System.out.println((currentColumn + n)>-1?(currentColumn+n)%dimensionsNumber : dimensionsNumber-1);
                currentState = transition.to;
                char currentLetter = transition.letter;
                if (currentLetter == 'l') {
                    if (n != 0) {
                        m = -n;
                        n = 0;
                    } else {
                        n = m;
                        m = 0;
                    }
                } else if (currentLetter == 'r') {
                    if (n != 0) {
                        m = n;
                        n = 0;
                    } else {
                        n=-m;
                        m=0;
                    }
                } else if (currentLetter == 'm') {
                    currentRow = (currentRow + m)>-1?(currentRow+m)%dimensionsNumber : dimensionsNumber-1;
                    currentColumn =  (currentColumn + n) >-1?(currentColumn+n)%dimensionsNumber : dimensionsNumber-1;
                    if (currentInput[currentRow][currentColumn]=='*'){
                        applesNumber++;
                    }
                    currentInput[currentRow][currentColumn]='.';
                }
            }
            if (applesNumber==maxApplesNumber){
                break;
            }
            tactsNumber++;
        }
        PrintWriter pw = new PrintWriter("ant.out");
        for (int i=0; i<automatonStates; i++){
            pw.println(String.format("%d %d %c %c", automaton.states[i].noApple.to+1, automaton.states[i].ifApple.to+1,
                    automaton.states[i].noApple.letter,automaton.states[i].ifApple.letter) );
        }
        pw.close();
    }

    public static class Automaton {
        State[] states;
        ArrayList<Character> validCharacters = new ArrayList<Character>();

        public Automaton(int statesNumber) {
            states = new State[statesNumber];
            validCharacters.add('r');
            validCharacters.add('l');
            validCharacters.add('m');
        }

        State genState(long number, int stateNumber) {
            int count = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    for (int k = 0; k < states.length; k++) {
                        for (int l = 0; l < states.length; l++) {
                            if (count == number) {
                                return new State(stateNumber, validCharacters.get(i), k, validCharacters.get(j), l);
                            }
                            count++;
                        }
                    }
                }
            }
            return null;
        }

        static class State {
            int number;
            Transition noApple;
            Transition ifApple;

            public State(int number, char letterNoApple, int noApple, char letterIfApple, int ifApple) {
                this.number = number;
                this.noApple = new Transition(letterNoApple, noApple);
                this.ifApple = new Transition(letterIfApple, ifApple);
            }

            static class Transition {
                char letter;
                int to;

                public Transition(char letter, int to) {
                    this.letter = letter;
                    this.to = to;
                }
            }
        }
    }
}
